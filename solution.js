import fs from "fs"

/**
 * find the name corresponding to givenId
 * @param {{givenId, path}} 
 */
export const findNameById = async ({ givenId, path }) => {
  let chunkRest = null;
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(path)) {
      return reject(new Error("File not found"));
    }
    if (!givenId || Number.isNaN(parseInt(givenId))) {
      return reject(new Error("Given id is not a number"));
    }
    const stream = fs.createReadStream(path);
    stream
      .on("data", (d) => {
        const response = getListOfObjects(d.toString(), chunkRest);
        chunkRest = response.chunkRest;
        const obj = response.result.find((e) => e.id === parseInt(givenId));
        if (obj) {
          stream.close();
          return resolve(obj.name);
        }
      })
      .on("end", () => {
        return reject(new Error("No match found"))
      });
  });
}

/**
 * Extracts a list of objects from a string
 * Then return the rest of the string that was not a valid json object in chunkRest
Neque aperiam magni 
 * @returns {result: Array, chunkRest: String}
 */
export const getListOfObjects = (str, chunkRest) => {
  let firstBracket, lastBracket, objects;

  if (!str) {
    return { result: null, chunkRest: null };
  }

  // If there is a chunkRest, we concatenate it with the new chunk (str), and then we start from the beginning
  if (chunkRest) {
    const newStr = chunkRest + str;
    chunkRest = null;
    return getListOfObjects(newStr, chunkRest);
  }

  firstBracket = str.indexOf("{", firstBracket + 1);
  lastBracket = str.lastIndexOf("}");
  if (lastBracket <= firstBracket) {
    return { result: null, chunkRest: str };
  }
  objects = str.substring(firstBracket, lastBracket + 1);
  try {
    chunkRest = str.substring(lastBracket + 1);
    return { result: JSON.parse(`[${objects}]`), chunkRest: chunkRest === "" ? null : chunkRest };
  } catch {
    return { result: null, chunkRest: str };
  }
}

(async () => {
  try {
    const name = await findNameById({ givenId: process.argv[2], path: "./data/input.json" });
    console.log(name);
  } catch (error) {
    console.log(error.message);
  }
})();

