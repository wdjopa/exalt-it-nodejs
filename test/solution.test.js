import { getListOfObjects, findNameById } from "../solution.js";

describe("getListOfObjects Unit Tests Suite", () => {
  it("should return {result : null, chunkRest : null}", () => {
    const response = getListOfObjects();
    expect(response).toEqual({ result: null, chunkRest: null });
  });
  it("should return {result : null, chunkRest : 'Hello'}", () => {
    const response = getListOfObjects("Hello");
    expect(response).toEqual({ result: null, chunkRest: "Hello" });
  });
  it("should return {result : [{bonjour: hello}], chunkRest : null}", () => {
    const response = getListOfObjects('{"bonjour": "hello"}');
    expect(response).toEqual({ result: JSON.parse('[{"bonjour": "hello"}]'), chunkRest: null });
  });
  it('should return {result : [{bonjour: hello}], chunkRest : ", {Comment: tu vas"}', () => {
    const response = getListOfObjects('{"bonjour": "hello"}, {"Comment", "tu vas"');
    expect(response).toEqual({ result: JSON.parse('[{"bonjour": "hello"}]'), chunkRest: ', {"Comment", "tu vas"' });
  });
  it('should return {result : null, chunkRest : ", {Comment: tu vas},"}', () => {
    const response = getListOfObjects("},", ', {"Comment", "tu vas"');
    expect(response).toEqual({ result: null, chunkRest: ', {"Comment", "tu vas"},' });
  });
});

describe("findNameById Unit Tests Suite", () => {
  it("Should throw Given id is not a number", async () => {
    await expect(findNameById({ givenId: "eliot", path: "./data/input.json" })).rejects.toThrow("Given id is not a number");
  });

  it("Should throw File not found", async () => {
    await expect(findNameById({ givenId: 1, path: "./data/not_exist.json" })).rejects.toThrow("File not found");
  });

  it("Should return 'Trevion Lynch'", async () => {
    await expect(findNameById({ givenId: 1, path: "./data/input.json" })).resolves.toEqual("Trevion Lynch");
  });

  it("Should return 'Jailyn Bartoletti'", async () => {
    await expect(findNameById({ givenId: 597091, path: "./data/input.json" })).resolves.toEqual("Jailyn Bartoletti");
  });

  it("Should return 'No match found'", async () => {
    await expect(findNameById({ givenId: 5970918865, path: "./data/input.json" })).rejects.toThrow("No match found");
  });
});
